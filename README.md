Micropython firmware build for ESP8266 in Docker
================================================

This Docker image can be used to build custom firmware with own freezed modules. All dependencies and Micropython itself are there already compiled (takes a lot of time), so you can just pull image in specific version, add your own modules and build firmware in a moment.

## How to use

Start container:

```
docker run -it --rm -v $PWD:/app registry.gitlab.com/janpoboril/micropython-docker-build/esp8266:latest bash
```

Copy you code to module, make and copy firmware back to host:

```
cd /micropython/ports/esp8266
cp -R /app/src/* modules
make
mv build-GENERIC/firmware-combined.bin /app
```

### Tags

All [versions of Micropython](https://github.com/micropython/micropython/tags) later than v1.10 are available as [Docker image tags](https://gitlab.com/janpoboril/micropython-docker-build/container_registry), so you can use always `latest` or pin to specific version.

## Contributions

If you want to fix or improve something you are welcome - fork project and send merge request. Especially support for another boards or forks is welcome.

Inspired by: https://github.com/fcollova/micropython-docker-build
