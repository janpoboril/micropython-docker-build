FROM larsks/esp-open-sdk

ARG VERSION=master
RUN git clone --depth 1 --branch $VERSION https://github.com/micropython/micropython.git /micropython

RUN cd /micropython/ports/esp8266 && make submodules
RUN cd /micropython/mpy-cross && make
RUN cd /micropython/ports/esp8266 && make

# Now there are all sources precompiled, so when you pull this image and add your custom modules, firmware build will be faster.

WORKDIR /app
